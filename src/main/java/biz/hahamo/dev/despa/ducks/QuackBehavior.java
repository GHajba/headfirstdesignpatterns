package biz.hahamo.dev.despa.ducks;

public interface QuackBehavior {

    void quack();
}
