package biz.hahamo.dev.despa.ducks;

public class FlyWithWings implements FlyBehavior {

    public void fly() {
        System.out.println("I'm flying!");
    }
}
