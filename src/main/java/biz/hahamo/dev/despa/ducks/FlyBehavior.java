package biz.hahamo.dev.despa.ducks;

public interface FlyBehavior {
    
    void fly();
}
