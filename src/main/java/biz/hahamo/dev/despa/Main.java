package biz.hahamo.dev.despa;

import biz.hahamo.dev.despa.ducks.MiniDuckSimulator;
import biz.hahamo.dev.despa.game.MiniGameSimulator;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Main {
    // not the best solution but it's ok for now
    private static List<Class> apps = new ArrayList<Class>();
    static {
        apps.add(MiniDuckSimulator.class);
        apps.add(MiniGameSimulator.class);
    }
    public static void main(String... args) {
        for(Class app : apps) {
            String name = app.getSimpleName();
            System.out.println("==== " + name + " ====");
            try {
                Method main = app.getMethod("main");
                main.invoke(null, null);
                System.out.println();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            
        }
    }
}
