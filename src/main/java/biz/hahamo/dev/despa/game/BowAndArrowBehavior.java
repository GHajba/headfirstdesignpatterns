package biz.hahamo.dev.despa.game;

public class BowAndArrowBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("Bow and arrows.");
    }
}