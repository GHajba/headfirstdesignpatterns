package biz.hahamo.dev.despa.game;

public abstract class Character {
    WeaponBehavior weapon;
    
    void setWeapon(WeaponBehavior weapon) {
        this.weapon = weapon;
    }
    
    public abstract void fight();
}