package biz.hahamo.dev.despa.game;

public class MiniGameSimulator {
    public static void main() {
        Character troll = new Troll();
        troll.setWeapon(new AxeBehavior());
        Character knight = new Knight();
        knight.setWeapon(new SwordBehavior());
        
        troll.fight();
        knight.fight();
    }
}