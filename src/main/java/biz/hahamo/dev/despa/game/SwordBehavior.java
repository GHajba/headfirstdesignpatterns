package biz.hahamo.dev.despa.game;

public class SwordBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("Using a sword.");
    }
}