package biz.hahamo.dev.despa.game;

public interface WeaponBehavior {
    void useWeapon();
}