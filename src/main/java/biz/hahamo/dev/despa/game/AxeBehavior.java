package biz.hahamo.dev.despa.game;

public class AxeBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("Using an axe.");
    }
}