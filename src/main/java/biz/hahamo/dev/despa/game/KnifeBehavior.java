package biz.hahamo.dev.despa.game;

public class KnifeBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("Using a knife.");
    }
}